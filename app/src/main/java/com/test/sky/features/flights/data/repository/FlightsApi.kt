package com.test.sky.features.flights.data.repository

import com.test.sky.features.flights.data.model.Flights
import io.reactivex.Observable
import retrofit2.http.GET

interface FlightsApi {

    @GET("flights.json")
    fun fetchFlights(): Observable<Flights>
}
