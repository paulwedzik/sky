package com.test.sky.features.flights.presentation

import com.test.sky.features.flights.domain.model.FlightItineraryModel

data class FlightsViewState(
    val isLoading: Boolean = false,
    val isError: Boolean = false,
    val flights: List<FlightItineraryModel> = emptyList()
)
