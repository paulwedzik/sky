package com.test.sky.features.flights.presentation

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.test.sky.R
import com.test.sky.features.flights.domain.model.FlightItineraryModel
import com.test.sky.features.flights.presentation.FlightsIntent.GetFlights
import com.test.sky.features.flights.presentation.adapter.FlightItineraryAdapter
import dagger.android.AndroidInjection
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import kotlinx.android.synthetic.main.activity_main.*
import timber.log.Timber
import javax.inject.Inject

class FlightsActivity : AppCompatActivity(), HasAndroidInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private var flightsAdapter = FlightItineraryAdapter()

    private lateinit var viewModel: FlightsViewModel

    override fun androidInjector() = dispatchingAndroidInjector

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AndroidInjection.inject(this)
        viewModel = ViewModelProviders.of(this, viewModelFactory)[FlightsViewModel::class.java]
        setContentView(R.layout.activity_main)
        recyclerViewFlights.adapter = flightsAdapter
        initViewModel()
    }

    private fun initViewModel() {
        viewModel.viewState.observe(this, Observer { viewState ->
            renderState(viewState)
        })
        viewModel.dispatchIntent(GetFlights)
    }

    private fun renderState(viewState: FlightsViewState) {
        when {
            viewState.isLoading -> showLoading()
            viewState.isError -> showError()
            viewState.flights.isNotEmpty() -> showNews(viewState.flights)
        }
    }

    private fun showLoading() {
        recyclerViewFlights.visibility = View.GONE
        textViewError.visibility = View.GONE
        progressBar.visibility = View.VISIBLE
    }

    private fun showError() {
        progressBar.visibility = View.GONE
        textViewError.visibility = View.VISIBLE
    }

    private fun showNews(flights: List<FlightItineraryModel>) {
        progressBar.visibility = View.GONE
        recyclerViewFlights.visibility = View.VISIBLE
        flightsAdapter.flightsList = flights
        flightsAdapter.notifyDataSetChanged()
    }
}
