package com.test.sky.features.flights.domain

import com.test.sky.features.flights.domain.model.FlightItineraryModel

sealed class FlightsResult {
    object Loading : FlightsResult()
    object Error : FlightsResult()
    data class Data(val flights: List<FlightItineraryModel>) : FlightsResult()
}
