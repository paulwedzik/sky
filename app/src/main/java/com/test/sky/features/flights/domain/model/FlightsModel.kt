package com.test.sky.features.flights.domain.model

data class FlightLegModel(
    val iconUrl: String,
    val departureArrivalTime: String,
    val additionalInfo: String,
    val time: String,
    val stops: Int
)
