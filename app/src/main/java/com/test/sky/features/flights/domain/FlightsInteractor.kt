package com.test.sky.features.flights.domain

import com.test.sky.features.flights.data.repository.FlightsRepository
import io.reactivex.Observable
import timber.log.Timber
import javax.inject.Inject

class FlightsInteractor @Inject constructor(
    private val repository: FlightsRepository,
    private val flightModelMapper: FlightModelMapper
) {

    fun getFlights(): Observable<FlightsResult> = repository.getFlights()
        .map<FlightsResult> { FlightsResult.Data(flightModelMapper.map(it)) }
        .onErrorReturn {
            Timber.e(it)
            FlightsResult.Error
        }
        .startWith(FlightsResult.Loading)

}
