package com.test.sky.features.flights.presentation

import com.test.sky.base.BaseViewModel
import com.test.sky.base.SchedulersProvider
import com.test.sky.features.flights.domain.FlightsInteractor
import com.test.sky.features.flights.domain.FlightsResult
import io.reactivex.Observable
import javax.inject.Inject

class FlightsViewModel @Inject constructor(
    schedulersProvider: SchedulersProvider,
    private val flightsInteractor: FlightsInteractor
) :
    BaseViewModel<FlightsResult, FlightsViewState, FlightsIntent>(schedulersProvider) {

    override fun handleIntent(intent: FlightsIntent): Observable<FlightsResult> = when (intent) {
        is FlightsIntent.GetFlights -> flightsInteractor.getFlights()
    }

    override fun reduceViewState(result: FlightsResult): FlightsViewState = when (result) {
        FlightsResult.Loading -> FlightsViewState(isLoading = true)
        FlightsResult.Error -> FlightsViewState(isError = true)
        is FlightsResult.Data -> FlightsViewState(flights = result.flights)
    }
}
