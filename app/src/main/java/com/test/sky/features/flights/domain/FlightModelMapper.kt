package com.test.sky.features.flights.domain

import com.test.sky.features.flights.data.model.Flights
import com.test.sky.features.flights.data.model.Itinerary
import com.test.sky.features.flights.data.model.Leg
import com.test.sky.features.flights.domain.model.FlightItineraryModel
import com.test.sky.features.flights.domain.model.FlightLegModel
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import kotlin.math.min

class FlightModelMapper @Inject constructor() {

    fun map(flights: Flights): List<FlightItineraryModel> {
        val flightItineraries = mutableListOf<FlightItineraryModel>()
        flights.itineraries.forEach { itinerary ->
            val flightLegs = createItineraryModel(itinerary, flights)
            flightItineraries.add(FlightItineraryModel(itinerary.price, itinerary.agent, flightLegs))
        }
        return flightItineraries
    }

    private fun createItineraryModel(itinerary: Itinerary, flights: Flights): MutableList<FlightLegModel> {
        val flightLegs = mutableListOf<FlightLegModel>()
        val legs = getLegsForIds(itinerary.legsIds, flights)
        legs.forEach { flightLegs.add(createFlightLegModel(it)) }
        return flightLegs
    }

    private fun createFlightLegModel(leg: Leg): FlightLegModel {
        val departureTime = getDepartureTime(leg.departureTime)
        val arrivalTime = getDepartureTime(leg.arrivalTime)
        val departureArrivalTime = "$departureTime - $arrivalTime"
        val fromTo = "${leg.departureAirport}-${leg.arrivalAirport}"
        val additionalInfo = "$fromTo, ${leg.airlineName}"
        val hours = leg.durationMinutes / MINUTES_IN_HOUR
        val minutes = leg.durationMinutes % MINUTES_IN_HOUR
        val travelTime = "$hours h $minutes min"
        return FlightLegModel("", departureArrivalTime, additionalInfo, travelTime, leg.stops)
    }

    private fun getDepartureTime(departureTime: String): String {
        val simpleDateFormat = SimpleDateFormat(DATE_FORMAT, Locale.ENGLISH)
        val date = simpleDateFormat.parse(departureTime)
        val timeDateFormat = SimpleDateFormat(TIME_FORMAT, Locale.ENGLISH)
        return timeDateFormat.format(date)
    }

    private fun getLegsForIds(legsIds: List<String>, flights: Flights): List<Leg> {
        return flights.legs.filter { legsIds.contains(it.id) }
    }

    companion object {
        const val DATE_FORMAT = "yyyy-MM-dd'T'HH:mm"
        const val TIME_FORMAT = "HH:mm"
        const val MINUTES_IN_HOUR = 60
    }
}
