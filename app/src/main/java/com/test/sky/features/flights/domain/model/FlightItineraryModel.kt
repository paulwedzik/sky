package com.test.sky.features.flights.domain.model

data class FlightItineraryModel(
    val price: String,
    val agent: String,
    val flightLegs: List<FlightLegModel>
)
