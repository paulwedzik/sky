package com.test.sky.features.flights.presentation

/**
 * Intents for [FlightsViewModel]
 * If there's only one intent like here sealed class is excessive and could be skipped, but
 * from the architecture point of view it shows that all the intents go here
 */
sealed class FlightsIntent {
    object GetFlights : FlightsIntent()
    // Add other intents for login screen here
}
