package com.test.sky.features.flights.presentation.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.test.sky.R
import com.test.sky.features.flights.domain.model.FlightItineraryModel
import kotlinx.android.synthetic.main.layout_flight_internary.view.*

class FlightItineraryAdapter : RecyclerView.Adapter<FlightItineraryAdapter.ViewHolder>() {

    private val viewPool = RecyclerView.RecycledViewPool()

    var flightsList: List<FlightItineraryModel> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.layout_flight_internary, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return flightsList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val flight = flightsList[position]
        holder.view.recyclerViewLeg.apply {
            layoutManager = LinearLayoutManager(holder.view.context, RecyclerView.VERTICAL, false)
            adapter = FlightLegsAdapter(flight.flightLegs)
            setRecycledViewPool(viewPool)
        }
        holder.view.textViewPrice.text = flight.price
        holder.view.textViewAgent.text = flight.agent
    }

    inner class ViewHolder(val view: View
    ) : RecyclerView.ViewHolder(view)
}
