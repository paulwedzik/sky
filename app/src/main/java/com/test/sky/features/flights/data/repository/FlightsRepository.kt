package com.test.sky.features.flights.data.repository

import javax.inject.Inject

class FlightsRepository @Inject constructor(private val flightsApi: FlightsApi) {

    fun getFlights() = flightsApi.fetchFlights()
}
