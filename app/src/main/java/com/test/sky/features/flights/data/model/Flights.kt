package com.test.sky.features.flights.data.model

data class Flights(
    val itineraries: List<Itinerary>,
    val legs: List<Leg>
)
