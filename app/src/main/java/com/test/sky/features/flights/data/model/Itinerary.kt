package com.test.sky.features.flights.data.model

import com.google.gson.annotations.SerializedName

data class Itinerary(
    val id: String,
    @SerializedName("legs")
    val legsIds: List<String>,
    val price: String,
    val agent: String,
    val agent_rating: Float
)
