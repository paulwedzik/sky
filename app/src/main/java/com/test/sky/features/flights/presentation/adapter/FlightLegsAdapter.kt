package com.test.sky.features.flights.presentation.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.test.sky.R
import com.test.sky.features.flights.domain.model.FlightLegModel
import kotlinx.android.synthetic.main.layout_itiretary_leg.view.*
import timber.log.Timber

class FlightLegsAdapter(
    private val flightLegs: List<FlightLegModel>
) : RecyclerView.Adapter<FlightLegsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.layout_itiretary_leg, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount() = flightLegs.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val child = flightLegs[position]
        holder.view.textViewTime.text = child.departureArrivalTime
        holder.view.textViewDuration.text = child.time
        holder.view.textViewFromTo.text = child.additionalInfo
        holder.view.textViewStops.text = "${child.stops} stops"

        // I run out of time to load images :(
//        GlideApp.with(holder.imageView)
//            .load(child)
//            .into(holder.imageView)
    }

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view)
}
