package com.test.sky.di

import com.test.sky.features.flights.presentation.FlightsActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Module for contributing app activities
 */
@Suppress("unused")
@Module
abstract class ActivityModule {
    @ContributesAndroidInjector
    abstract fun contributeMainActivity(): FlightsActivity
}
